"
" Asyncomplete servers
"
"
""if has('python3')
""    let g:UltiSnipsExpandTrigger="<c-e>"
""    call asyncomplete#register_source(asyncomplete#sources#ultisnips#get_source_options({
""        \ 'name': 'ultisnips',
""        \ 'allowlist': ['*'],
""        \ 'completor': function('asyncomplete#sources#ultisnips#completor'),
""        \ }))
""endif


"
" Language
"

" python
"if executable('pylsp')
"    au User lsp_setup call lsp#register_server({
"        \ 'name': 'pylsp',
"        \ 'cmd': {server_info->['pylsp']},
"        \ 'allowlist': ['python'],
"        \ })
"endif

" bash
"if executable('bash-language-server')
"    au User lsp_setup call lsp#register_server({
"          \ 'name': 'bash-language-server',
"          \ 'cmd': {server_info->[&shell, &shellcmdflag, 'bash-language-server start']},
"          \ 'allowlist': ['sh'],
"          \ })
"endif

" rst
"if executable('esbonio')
"    au User lsp_setup call lsp#register_server({
"          \ 'name': 'esbonio',
"          \ 'cmd': {server_info->['esbonio']},
"          \ 'allowlist': ['rst'],
"          \ })
"    " https://swyddfa.github.io/esbonio/docs/extensions/tutorial.html
"endif

" terraform
"if executable('terraform-ls')
"    au User lsp_setup call lsp#register_server({
"        \ 'name': 'terraform-ls',
"        \ 'cmd': {server_info->['terraform-ls', 'serve']},
"        \ 'config': {'experimentalFeatures.prefillRequiredFields': 1},
"        \ 'allowlist': ['tf', 'terraform'],
"        \ })
"endif
"au User lsp_setup call lsp#register_server({
"    \ 'name': 'terraform-lsp',
"    \ 'cmd': {server_info->['terraform-lsp']},
"    \ 'allowlist': ['tf', 'terraform'],
"    \ })
"if executable('terraform')
"    au User lsp_setup call lsp#register_server({
"        \ 'name': 'terraform',
"        \ 'cmd': {server_info->['terraform']},
"        \ 'allowlist': ['tf', 'terraform'],
"        \ })
"endif

" golang
"if executable('gopls')
"    au User lsp_setup call lsp#register_server({
"        \ 'name': 'go-lang',
"        \ 'cmd': {server_info->['gopls']},
"        \ 'allowlist': ['go'],
"        \ })
"endif

if executable('zk')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'zk',
        \ 'cmd': ['zk', 'lsp'],
        \ 'allowlist': ['markdown'],
        \ })
endif

if executable('jsonnet-language-server')
    au User lsp_setup call lsp#register_server({
      \ 'name': 'jsonnet',
      \ 'cmd': {server_info->lsp_settings#get('jsonnet-language-server', 'cmd', [lsp_settings#exec_path('jsonnet-language-server')]+lsp_settings#get('jsonnet-language-server', 'args', ['-t']))},
      \ 'root_uri':{server_info->lsp_settings#get('jsonnet-language-server', 'root_uri', lsp_settings#root_uri('jsonnet-language-server'))},
      \ 'initialization_options': lsp_settings#get('jsonnet-language-server', 'initialization_options', v:null),
      \ 'allowlist': lsp_settings#get('jsonnet-language-server', 'allowlist', ['jsonnet']),
      \ 'blocklist': lsp_settings#get('jsonnet-language-server', 'blocklist', []),
      \ 'config': lsp_settings#get('jsonnet-language-server', 'config', lsp_settings#server_config('jsonnet-language-server')),
      \ 'workspace_config': lsp_settings#get('jsonnet-language-server', 'workspace_config', {
      \   'jsonnet': {'lint': {'validProperties': []}},
      \ }),
      \ 'semantic_highlight': lsp_settings#get('jsonnet-language-server', 'semantic_highlight', {}),
      \ })
endif


if executable('vim-language-server')
    au User lsp_setup call lsp#register_server({
      \ 'name': 'vim-language-server',
      \ 'cmd': {server_info->lsp_settings#get('vim-language-server', 'cmd', [lsp_settings#exec_path('vim-language-server')]+lsp_settings#get('vim-language-server', 'args', ['--stdio']))},
      \ 'root_uri':{server_info->lsp_settings#get('vim-language-server', 'root_uri', lsp_settings#root_uri('vim-language-server'))},
      \ 'initialization_options': extend({
      \   'isNeovim': has('nvim'),
      \   'vimruntime': $VIMRUNTIME,
      \   'runtimepath': &rtp,
      \   'iskeyword': &isk . ',:',
      \   'diagnostic': {'enable': v:true}
      \  }, lsp_settings#get('vim-language-server', 'initialization_options', {}), 'force'),
      \ 'allowlist': lsp_settings#get('vim-language-server', 'allowlist', ['vim']),
      \ 'blocklist': lsp_settings#get('vimbash-language-server', 'blocklist', []),
      \ 'config': lsp_settings#get('vim-language-server', 'config', lsp_settings#server_config('vim-language-server')),
      \ 'workspace_config': lsp_settings#get('vim-language-server', 'workspace_config', {}),
      \ 'semantic_highlight': lsp_settings#get('vim-language-server', 'semantic_highlight', {}),
      \ })
endif

"
" Utilities
"

" Filesystem path
au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#file#get_source_options({
    \ 'name': 'file',
    \ 'allowlist': ['*'],
    \ 'priority': 10,
    \ 'completor': function('asyncomplete#sources#file#completor')
    \ }))


" From buffers
au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#buffer#get_source_options({
    \ 'name': 'buffer',
    \ 'allowlist': ['*'],
    \ 'blocklist': ['go'],
    \ 'completor': function('asyncomplete#sources#buffer#completor'),
    \ 'config': {
    \    'max_buffer_size': 5000000,
    \  },
    \ }))

" From tmux
let g:tmuxcomplete#asyncomplete_source_options = {
            \ 'name':      'tmuxcomplete',
            \ 'whitelist': ['*'],
            \ 'config': {
            \     'splitmode':      'ilines,words',
            \     'filter_prefix':   1,
            \     'show_incomplete': 1,
            \     'sort_candidates': 0,
            \     'scrollback':      0,
            \     'truncate':        0
            \     }
            \ }

" Snippets
"let g:UltiSnipsExpandTrigger="<c-e>"
call asyncomplete#register_source(asyncomplete#sources#ultisnips#get_source_options({
    \ 'name': 'ultisnips',
    \ 'allowlist': ['*'],
    \ 'completor': function('asyncomplete#sources#ultisnips#completor'),
    \ }))

" More:
" https://github.com/prabirshrestha/asyncomplete-tags.vim

if executable('nil')
  autocmd User lsp_setup call lsp#register_server({
    \ 'name': 'nil',
    \ 'cmd': {server_info->['nil']},
    \ 'whitelist': ['nix'],
    \ })
endif
