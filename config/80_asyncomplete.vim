"
" Asyncomplete default setup.
"

" Debug parameters
" Just run vim with environment variable VIM_lsp_debug=true
if $VIM_lsp_debug == "true"
  let g:lsp_diagnostics_enabled = 1
  let g:lsp_log_verbose = 1
  let g:lsp_log_file = expand('~/vim-lsp.log')
  " for asyncomplete.vim log
  let g:asyncomplete_log_file = expand('~/asyncomplete.log')
endif


" Autocompletition while typeing
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"

let g:asyncomplete_auto_popup = 1


function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ asyncomplete#force_refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"


" allow modifying the completeopt variable, or it will
" be overridden all the time
let g:asyncomplete_auto_completeopt = 0
set completeopt=menuone,noinsert,noselect,preview

let g:lsp_diagnostics_echo_cursor = 1

"
" To auto close preview window when completion is done.
"
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif


function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> <leader>gd <plug>(lsp-definition)
    nmap <buffer> <leader>gs <plug>(lsp-document-symbol-search)
    nmap <buffer> <leader>gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> <leader>gr <plug>(lsp-references)
    nmap <buffer> <leader>gi <plug>(lsp-implementation)
    nmap <buffer> <leader>gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> <leader>[g <plug>(lsp-previous-diagnostic)
    nmap <buffer> <leader>]g <plug>(lsp-next-diagnostic)
    nmap <buffer> <leader>K <plug>(lsp-hover)
    nnoremap <buffer> <leader><expr><c-f> lsp#scroll(+4)
    nnoremap <buffer> <leader><expr><c-d> lsp#scroll(-4)

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')
    " refer to doc to add more commands
endfunction

augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END
" Shortcut
" nmap \gd :LspDefinition<cr>
" nmap \gt :tab split<cr>:LspDefinition<cr>
" nmap \gs :sp<cr>:LspDefinition<cr>
" nmap \gv :vsp<cr>:LspDefinition<cr>
" :leftabove LspDefinition Splits horizontally and opens a window to the above.
" :rightbelow vertical LspDefinition Splits vertically and opens a window to the right.
" :tab LspDefinition Opens a window to the new tabpage.

" More nice documentation:
" https://jdhao.github.io/2020/11/04/replace_deoplete_jedi_for_LSP/
