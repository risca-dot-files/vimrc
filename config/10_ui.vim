" show matching brackets/parenthesis
set showmatch

" disable startup message
set shortmess+=I

" hide mode display
"set noshowmode


" If terminal is missing a solarized theme go for 256:
"let g:solarized_termcolors=256
"set t_Co=256
"set t_Co=16
"let g:solarized_termcolors=16
" syntax highlighting
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48:2:%lu:%lu:%lum"

" CURSOR SHAPE and COLOR
let &t_SI .= "\<Esc>[5 q"
" solid block
let &t_EI .= "\<Esc>[1 q"
"Ps = 0  -> blinking block.
"Ps = 1  -> blinking block (default).
"Ps = 2  -> steady block.
"Ps = 3  -> blinking underline.
"Ps = 4  -> steady underline.
"Ps = 5  -> blinking bar (xterm).
"Ps = 6  -> steady bar (xterm).
" see more at https://vim.fandom.com/wiki/Configuring_the_cursor

"let &t_ut=''
set termguicolors
set background=dark
colorscheme solarized8
"colorscheme selenized
syntax enable
set synmaxcol=512
filetype plugin on

" stop unnecessary rendering
"set lazyredraw

" show line numbers
set number
"set relativenumber

" default no line wrapping
set nowrap

" set indents when wrapped
set breakindent

" no folding
"set nofoldenable
"set foldlevel=99
"set foldminlines=99
"set foldlevelstart=99

" highlight cursor
set cursorline
"set cursorcolumn

" show invisibles
set list
set listchars=
set listchars+=tab:·\ 
set listchars+=trail:·
set listchars+=extends:»
set listchars+=precedes:«
set listchars+=nbsp:░

" split style
set fillchars=vert:▒

" tree style file explorer
let g:netrw_liststyle=3
let g:netrw_browse_split=4
let g:netrw_winsize=25

" darken inactive panes
" hi SignColumn   ctermbg=234
" hi ActiveWindow ctermbg=0 | hi InactiveWindow ctermbg=234
" set winhighlight=Normal:ActiveWindow,NormalNC:InactiveWindow

" If running under sway, use wl-paste and wl-copy
if $XDG_CURRENT_DESKTOP == "sway"
  xnoremap "+y y:call system("wl-copy", @")<cr>
  nnoremap "+p :let @"=substitute(system("wl-paste --no-newline"), '<C-v><C-m>', '', 'g')<cr>p
  nnoremap "*p :let @"=substitute(system("wl-paste --no-newline --primary"), '<C-v><C-m>', '', 'g')<cr>p
endif
