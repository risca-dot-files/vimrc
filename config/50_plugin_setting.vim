

"""""""""""""""
"-- tagbar
"""""""""""""""
nmap <F8> :TagbarToggle<CR>


""""""""""""""""""
"-- vim-open-url
""""""""""""""""""
let g:open_url_browser_default = 'firefox'
let g:open_url_default_mappings = 0
nmap gB :OpenURLFind<CR>


"""""""""""""""
"-- ALE --
"""""""""""""""
"hi clear ALEErrorSign
"hi clear ALEWarningSign
let g:ale_echo_msg_format = '[%linter%] %s %code% [%severity%]'
" %code: %%s
let g:ale_sign_error = '●'
let g:ale_sign_warning = '.'
"hi Error    ctermfg=204 ctermbg=NONE guifg=#ff5f87 guibg=NONE
"hi Warning  ctermfg=178 ctermbg=NONE guifg=#D7AF00 guibg=NONE
"hi ALEError ctermfg=204 guifg=#ff5f87 ctermbg=52 guibg=#5f0000 cterm=undercurl gui=undercurl
"hi link ALEErrorSign    Error
"hi link ALEWarningSign  Warning

let g:ale_linters = {
            \ 'python': ['pylint'],
            \ 'terraform': ['tflint'],
            \ }
"            \ 'tf': ['terraform'],
"             \ 'javascript': ['eslint'],
"             \ 'go': ['gobuild', 'gofmt'],
"             \ 'rust': ['rls']
"             \}
" let g:ale_fixers = {
"             \ '*': ['remove_trailing_lines', 'trim_whitespace'],
"             \ 'python': ['autopep8'],
"             \ 'javascript': ['eslint'],
"             \ 'go': ['gofmt', 'goimports'],
"             \ 'rust': ['rustfmt']
"             \}
"
let g:ale_close_preview_on_insert=1
let g:ale_open_list=1
let g:ale_list_vertical=0
augroup CloseLoclistWindowGroup
    autocmd!
    autocmd QuitPre * if empty(&buftype) | lclose | endif
augroup END
" nmap <silent> <C-e> <Plug>(ale_next_wrap)

" let g:ale_fix_on_save = 0



"""""""""""""""""""""""""""
"-- vim-table-mode
"""""""""""""""""""""""""""
" rst alike tables:
let g:table_mode_corner_corner='+'
let g:table_mode_header_fillchar='='



" Status line
"function! LinterStatus() abort
"    let l:counts = ale#statusline#Count(bufnr(''))    let l:all_errors = l:counts.error + l:counts.style_error
"    let l:all_non_errors = l:counts.total - l:all_errors    return l:counts.total == 0 ? 'OK' : printf(
"        \   '%d⨉ %d⚠ ',
"        \   all_non_errors,
"        \   all_errors
"        \)
"endfunction
"set statusline+=%=
"set statusline+=\ %{LinterStatus()}

" Load all plugins now.
" Plugins need to be added to runtimepath before helptags can be generated.
packloadall
" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
silent! helptags ALL
