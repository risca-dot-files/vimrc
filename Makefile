# This is a makefile to help vim docs and setup management.
#
# Yes, we had a complexity problem to manage the current project, so we have
# just had an extra complex tool in order to manage the initial complexity.

.ONESHELL:
.SHELLFLAGS = -ec

ENV ?= dev## Environment. Default is dev: dev has developing tools and test utilities
VENV = venv
REQ = requirements.txt

ifeq ($(ENV),dev)
REQS := requirements.dev.txt requirements.local.txt
else
REQS :=
endif


# How to activate venv
ON_VENV = . $(VENV)/bin/activate
PYTHON = ${VENV}/bin/python3
PYVER ?= 3.8## Python version for virtualenv (default to 3.8)


.PHONY: help
help:  ## Print current help documentation
	@echo "\n  Makefile of $$(pwd)\n"
	@grep -E '^[a-z.A-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo "List of environment variables:"
	@grep -E '^[a-zA-Z_-]+ *\?= *[^ ]+## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ".=.*[a-zA-Z0-9]+## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


submodule.has_update:  ## Check upstreams update
	git submodule foreach git fetch origin master
	git submodule foreach git diff --stat HEAD origin/master
