
===============
TO BE EVALUATED
===============

general:
- https://github.com/tpope/vim-sensible
- https://github.com/tpope/vim-obsession
and more tpope!

UI
##

- https://github.com/itchyny/lightline.vim

- multicursors: https://github.com/mg979/vim-visual-multi
- text align: https://github.com/godlygeek/tabular

Tmux
....

- https://github.com/christoomey/vim-tmux-navigator

File managers
#############

- NERDtree -> bloated
- https://gist.github.com/danidiaz/37a69305e2ed3319bfff9631175c5d0f for netrw.txt
- https://github.com/tpope/vim-vinegar
- https://github.com/justinmk/vim-dirvish
- with also https://github.com/kristijanhusak/vim-dirvish-git
- and https://github.com/tpope/vim-eunuch
- good way to integrate ranger: https://github.com/voldikss/vim-floaterm#ranger
- pure vim script: https://github.com/lambdalisue/fern.vim


TAGS
####

Autogenerate on write/update: https://github.com/ludovicchabant/vim-gutentags


GIT
###

show in buffer diffs: https://github.com/airblade/vim-gitgutter
Git binding (but I have shell)...
https://github.com/tpope/vim-fugitive


CODING
######

Completion
..........

https://github.com/prabirshrestha/asyncomplete-tags.vim
https://github.com/prabirshrestha/asyncomplete-ultisnips.vim

https://github.com/honza/vim-snippets
https://github.com/SirVer/ultisnips
https://github.com/mattn/vim-lsp-settings

https://github.com/ycm-core/YouCompleteMe
- python based, really big

https://github.com/shougo/deoplete.nvim
- previous was neocomplete-vim
- next is:

https://github.com/neoclide/coc.nvim
- but based upon nodejs :-(

https://github.com/yegappan/lsp
- from vim9, young.

Git
...

https://vimawesome.com/plugin/gitv

jsonnet
.......

* https://github.com/dsabsay/vim-tanka

Nix
...

- https://github.com/LnL7/vim-nix

more
....

https://github.com/tpope/vim-unimpaired
https://github.com/kien/ctrlp.vim
(see ctrlp demo here: https://www.youtube.com/watch?v=9XrHk3xjYsw )
https://github.com/preservim/nerdcommenter
https://github.com/tpope/vim-commentary
https://github.com/frazrepo/vim-rainbow
https://github.com/terryma/vim-multiple-cursors
https://github.com/tpope/vim-surround
https://github.com/jiangmiao/auto-pairs
https://www.vim.org/scripts/script.php?script_id=39

https://vimawesome.com/plugin/dockerfile-vim

For interactive code from vim to tmux:
 - https://github.com/preservim/vimux
 - https://github.com/greghor/vim-pyShell

RST:
- https://github.com/chrisjsewell/rst-language-server
- https://github.com/gu-fan/InstantRst

sphinx:
- https://github.com/stsewd/sphinx.nvim

colors:
- https://github.com/KabbAmine/vCoolor.vim

Syntax
######

- https://github.com/preservim/vim-markdown (but now testing tpope/vim-markdown)

Other
#####

- https://vi.stackexchange.com/questions/23179/how-do-i-enter-into-vimdiff-mode-given-two-splits-are-already-open
- https://github.com/dkarter/bullets.vim


Search and moving:
https://github.com/mileszs/ack.vim  should be similar somehow to :Ag

- checkbox toggle: https://github.com/jkramer/vim-checkbox/blob/master/plugin/checkbox.vim

https://github.com/mbbill/undotree
https://github.com/tpope/vim-capslock


