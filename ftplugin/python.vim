" Python specific settings.
setlocal tabstop=4
setlocal softtabstop=4
setlocal shiftwidth=4
setlocal expandtab
setlocal autoindent
setlocal smarttab
setlocal formatoptions=croql
setlocal fileformat=unix
setlocal textwidth=99

if executable('pylsp')
    " pip install python-language-server
    " see: https://github.com/dense-analysis/ale/issues/3722
    " move to python-lsp-server
else
   echohl ErrorMsg
   echom "\"pylsp\" is not installed\n If you'd like autocompletion run :LspInstallServer and optionally \"pip install python-language-server\""
   echohl NONE
endif

"au BufRead,BufNewFile *.py,*.pyw,*.c,*.h match BadWhitespace /\s\+$/

