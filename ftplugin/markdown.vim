let g:markdown_syntax_conceal = 1
let g:markdown_folding = 1
set conceallevel=2
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh', 'javascript', 'jsonnet']

"if exists("b:current_syntax")
"  finish
"endif
"" Custom conceal
"syntax match todoCheckbox "\[\ \]" conceal cchar=
"syntax match todoCheckbox "\[x\]" conceal cchar=
""call matchadd('Conceal', '\[\ \]', 0, 11, {'conceal': ''})
""call matchadd('Conceal', '\[x\]', 0, 12, {'conceal': ''})
"let b:current_syntax = "todo"
"
"hi def link todoCheckbox Todo
"hi Conceal guibg=NONE
"
"setlocal cole=1
