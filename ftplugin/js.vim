"au BufNewFile,BufRead *.js, *.html, *.css
"    \ set tabstop=2
"    \ set softtabstop=2
"    \ set shiftwidth=2

setlocal tabstop=2
setlocal softtabstop=2
setlocal shiftwidth=2
