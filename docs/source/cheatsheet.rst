
==============
Vim Cheatsheet
==============

*****
Modes
*****

Vim modes:

- normal mode
- insert mode
- replace mode
- command mode


***************
File management
***************

.. table:: 
   :align: center
   :width: 95%

   ===================  =============================
   ``ZZ``               Exit and save (same as `:x`)
   ``ZQ``               Exit not saving (same as `:q!`)
   ``:w``               Write changes
   ``:w FILE``          Write on new FILE
   ``:n1,n2w FILE``     Write given line range content to FILE
   ``:n1,n2w >> FILE``  Append given line range content to FILE
   ``:e FILE``          Edit file
   ``:r FILE``          Read FILE and copy under cursor
   ``:r! <cmd>``        Read output of <cmd> and copy under cursor
   ===================  =============================

..

   To work on another file:
    *  {{{:n}}}: lavora sul file successivo
    *  {{{:N}}}: lavora sul file precedente
    *  {{{:rew}}}: torna al primo file

Windows
=======

========== ======== ==========================
:split     CRTL-W_s split
:vsplit    CRTL-W_v vertical split
========== ======== ==========================

============ ============ ==========================================
:close       CTRL-W_c     close window
============ ============ ==========================================

Window Resize:

======= ==== =====================
CTRL-W  `+`  more vertically
        `-`  less vertically
        `<`  more horizontally
        `>`  less horizontally
        `=`  equal split
======= ==== =====================

Otherwise use ``:resize``

Window Focus:

- ``Ctrl-W h/j/k/l``: move focus to the wanted direction
- ``{n}Ctrl-W h/j/k/l`` as above, {n} times

Window Move:

- ``Ctrl-W H/J/K/L``: move window to the wanted direction

Scrolling:
- set [no]scrollbind: scroll windows together


Tabs
====

.. code::

  vim -p first.txt second.txt
  
  :tabedit {file}   edit specified file in a new tab
  :tabfind {file}   open a new tab with filename given, searching the 'path' to find it
  :tabclose         close current tab
  :tabclose {i}     close i-th tab
  :tabonly          close all other tabs (show only the current tab)
  
  In addition to these commands, because Vim already has a plethora of commands for working with split windows, Vim provides the :tab command-line modifier, to use a new tab instead of a new window for commands that would normally split a window. For example:
  
  :tab ball         show each buffer in a tab (up to 'tabpagemax' tabs)
  :tab help         open a new help window in its own tab page
  :tab drop {file}  open {file} in a new tab, or jump to a window/tab containing the file if there is one
  :tab split        copy the current window to a new tab of its own
  
  Ctrl-W T   => move to a new tab
  
  :tabs         list all tabs including their displayed windows
  :tabm 0       move current tab to first
  :tabm         move current tab to last
  :tabm {i}     move current tab to position i+1
  
  :tabn         go to next tab
  :tabp         go to previous tab
  :tabfirst     go to first tab
  :tablast      go to last tab
  
  gt            go to next tab
  gT            go to previous tab
  {i}gt         go to tab in position i
  
  Ctrl-PgDn     go to next tab
  Ctrl-PgUp     go to previous tab

See more: `<http://vim.wikia.com/wiki/Using_tab_pages>`_

Cursor movement
===============

Text reference:

======  ===========================
hjkl    Left/Down/Up/Right
gg/G    inizio/fine file
%       move to matching bracket
======  ===========================

View reference
##############

Move cursor:

- H: move cursor to top of screen (High)
- M: move cursor to middle of screen (Middel)
- L: move cursor to bottom of screen (Low)

Move view (in relation to cursor):
- zt: to top
- zz or z.: to middle
- zb: to bottom

Move view:

- Ctrl-y: one line up
- Ctrl-e: one line down
- Ctrl-u: up half page
- Ctrl-d: down half page
- Ctrl-b: one screen before
- Ctrl-f: one screen forward


Search
######

- n   next matching search pattern
- ggn first ever
- N   previous matching search pattern
- GN  latest
- *   next whole word under cursor
- #   previous whole word under cursor
- g*  next matching search (not whole word) pattern under cursor
- g#  previous matching search (not whole word) pattern under cursor

Text editing
============

Searches
########

By entering ``/`` followed by the pattern.

- \c case insensitive
- \C case sensitive
- otherwise set ''ignorecase''

More options:
 *  :set (no)hlsearch highlight all recurrence

Substitutions
#############

For substitution:
-  :rs/foo/bar/a

Where:

- r [range] could be:

  - none: all file
  - n,m: from line n to line m

And extra flags:

- a: only first
- g: globally
- c: asks for confirmation
- i: ignore case
- I: don't ignore case

Language dictionary
###################

In order to enable a language checker:

.. code::

  :set spell spelllang=en_us         => attiva su tutta la sessione
  :setlocal spell spelllang=en_us    => attiva in localbuffer
  :set nospell

Now it will be possible to navigate through tags:

.. code::

  ]s    => go to next error
  [s    => go to previous error
  z=    => show suggestions

****
CTAG
****

* Ctrl+] - go to definition
* Ctrl+T - Jump back from the definition.
* Ctrl+W Ctrl+] - Open the definition in a horizontal split

In case of multiple definition:
* g] - show all recurrency
* :tn - round to next tag

For more fancies:

.. code::

  map <C-\> :tab split<CR>:exec("tag ".expand("<cword>"))<CR>
  map <A-]> :vsp <CR>:exec("tag ".expand("<cword>"))<CR>
  
  Ctrl+\ - Open the definition in a new tab
  Alt+] - Open the definition in a vertical split


**********
Webography
**********

- `guckes <http://www.guckes.net/talks/vim.small_commands.txt>`_
  - its `personal setup <http://www.guckes.net/vim/>`_

