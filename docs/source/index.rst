.. Vim docs documentation master file, created by
   sphinx-quickstart on Tue Mar 23 16:23:17 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Vim docs's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   cheatsheet
   plugin/index
   setup/index
   webography



Indices and tables
==================

* :ref:`search`
