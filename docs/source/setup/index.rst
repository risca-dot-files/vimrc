
##############
Setup overview
##############

How to check current keybinds:

- ``:map``: show custom keybinds
- ``:help index``: shortcut to help page for default keybind


*************
Nice commands
*************

Backup a copy on every file edit
================================


.. code::

  "au BufWritePre * let &backupext = '%' . substitute(expand("%:p:h"), "/" , "%" , "g") . "%" . strftime("%Y.%m.%d.%H.%M.%S")
  au BufWritePre * let &backupext = '~~' . strftime("%Y.%m.%d-%H.%M.%S")
  set backup
  set writebackup

