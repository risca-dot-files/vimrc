
===
FZF
===

Site: https://github.com/junegunn/fzf.vim

Fast search and filtering.

Examples:

- search file **:FZF**
- RipGrep folders **:Rg <regex>**

=======
Taglist
=======

**Requirements**: ctags installed on system.

Usage:

- {{{:TlistOpen}}} for opening tag window, then
- **space**: disply infos about tag
- **enter**: go to function
- **p**: go to in previous tab
- **P**: go to in new tab
- **ctl+t**: go back
- **ctl-]**: go to function
- **x**: zoom-in and zoom-out
- **+** or **-**: open/close a fold (***** or **=** for opening/closing all).

For more options see `the manual <http://vim-taglist.sourceforge.net/manual.html>`_
