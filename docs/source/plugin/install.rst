
Plugin management
=================

Install
+++++++

Tools:

- vim itself as ``pack``
- pathogen
- vundle

Helptags
++++++++

After new plugin installation, the following is require:

.. code::

   :helptags ALL


Pack
++++

Optional start:

- install under ``pack/<some>/opt/<name>``
- dinamically load via ``:packadd <plugin>``

