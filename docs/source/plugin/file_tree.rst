
====================
File tree navigation
====================

Interesting solution:

- plain vim (through netwr)
- netwr with some pluging
- dirvish (could be a little too basic)
- nerdtree (too bloated maybe)

Netwr
=====

Opening:

- t: open new tab
- o: open current tab

About where to open: https://stackoverflow.com/questions/58874545/netrw-open-file-in-the-netrw-buffer

Webography
++++++++++

- https://shapeshed.com/vim-netrw/
- https://gist.github.com/t-mart/610795fcf7998559ea80

.. note::
   Read more current webography (really nice command to better handle)

Nerdtree
========

Side buffer for exploring directories.

To launch it:

.. code::

  :NERDTree

Lot of custom integration such as:

- Nerdtree-git-plugin

Nice shortcuts:

- ``:NerdTreeFind`` to search for current file


Main commands
+++++++++++++

 * '''m''': menu, create new file or folder

----
 * '''o''': open file
 * '''s''': open with vsplit
 * '''go/s''': go open (open file but stay on nerdtree)
 * '''t''': open in new tab
 * '''T''': open in new tab but keep focus on current

----
 * '''C''': change the rootdir to current
 * '''p''': go to parent directory
 * '''x''': close parent directory
 * '''O''': open children directory recursively

----
 * '''A''': temporaly maximize the NERDTree window
 * '''q''': close NERTree window

----
 * '''I''': show hidden files
 * '''F''': show files

bookmarks
+++++++++

 * '''B''': show and hide bookmarks
 * {{{:Bookmark myname}}}: create new bookmark on hovering node
 * '''D'''  => delete current bookmark

Other options
+++++++++++++

To hide some files:

.. code::

  let NERDTreeIgnore = ['\.pyc$']

