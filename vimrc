"
" Load configurations file
"
for file in split(globpath(expand('~/.vim/config'), '*.vim'), '\n')
  if filereadable(file)
    execute 'source' file
  endif
endfor


"au BufWritePre * let &backupext = '%' . substitute(expand("%:p:h"), "/" , "%" , "g") . "%" . strftime("%Y.%m.%d.%H.%M.%S")
"au BufWritePre * let &backupext = '~~' . strftime("%Y.%m.%d-%H.%M.%S")
"set backup
"set writebackup
